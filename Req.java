package project;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

//	public static String URL ="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&name=cruise&key=YOUR_API_KEY";

public class Req {
	public static String URL ="https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
    public static String[] TYPE={"type=accounting",
    	"type=airport",
    	"type=amusement_park",
    	"type=aquarium",
    	"type=art_gallery",
    	"type=atm",
    	"type=bakery",
    	"type=bank",
    	"type=bar",
    	"type=beauty_salon",
    	"type=bicycle_store",
    	"type=book_store",
    	"type=bowling_alley",
    	"type=bus_station",
    	"type=cafe",
    	"type=campground",
    	"type=car_dealer",
    	"type=car_rental",
    	"type=car_repair",
    	"type=car_wash",
    	"type=casino",
    	"type=cemetery",
    	"type=church",
    	"type=city_hall",
    	"type=clothing_store",
    	"type=convenience_store",
    	"type=courthouse",
    	"type=dentist",
    	"type=department_store",
    	"type=doctor",
    	"type=electrician",
    	"type=electronics_store",
    	"type=embassy",
    	"type=establishment(deprecated)",
    	"type=finance(deprecated)",
    	"type=fire_station",
    	"type=florist",
    	"type=food(deprecated)",
    	"type=funeral_home",
    	"type=furniture_store",
    	"type=gas_station",
    	"type=general_contractor(deprecated)",
    	"type=grocery_or_supermarket",
    	"type=gym",
    	"type=hair_care",
    	"type=hardware_store",
    	"type=health(deprecated)",
    	"type=hindu_temple",
    	"type=home_goods_store",
    	"type=hospital",
    	"type=insurance_agency",
    	"type=jewelry_store",
    	"type=laundry",
    	"type=lawyer",
    	"type=library",
    	"type=liquor_store",
    	"type=local_government_office",
    	"type=locksmith",
    	"type=lodging",
    	"type=meal_delivery",
    	"type=meal_takeaway",
    	"type=mosque",
    	"type=movie_rental",
    	"type=movie_theater",
    	"type=moving_company",
    	"type=museum",
    	"type=night_club",
    	"type=painter",
    	"type=park",
    	"type=parking",
    	"type=pet_store",
    	"type=pharmacy",
    	"type=physiotherapist",
    	"type=place_of_worship(deprecated)",
    	"type=plumber",
    	"type=police",
    	"type=post_office",
    	"type=real_estate_agency",
    	"type=restaurant",
    	"type=roofing_contractor",
    	"type=rv_park",
    	"type=school",
    	"type=shoe_store",
    	"type=shopping_mall",
    	"type=spa",
    	"type=stadium",
    	"type=storage",
    	"type=store",
    	"type=subway_station",
    	"type=synagogue",
    	"type=taxi_stand",
    	"type=train_station",
    	"type=transit_station",
    	"type=travel_agency",
    	"type=university",
    	"type=veterinary_care",
    	"type=zoo"};
    public static String[] KEY={"key=AIzaSyBjVwkynplyDapjUM608_wepc7FWRQDkVc","key=AIzaSyDkuJS6G2WcBeNR40dHbF1RI2b35xlN_yA","key=AIzaSyCFKHx1aLGfvwgsUA1ha2KinoBf_5zTicc","key=AIzaSyATlIyh5NPe2y4jo7WnWoLyP3TOm0K0vB8","key=AIzaSyBMwrxSyCNCTu8XwL_PcMs9fG_zdaf08WA","key=AIzaSyCnVR3gFi3igcI6uDxS4tSTeSnYtj4kl-s","key=AIzaSyA6zaZHTX8qICa14hLd2KpMACWXVHyQf1I","key=AIzaSyD1-u0RxeIxWpIux-r5uRnQvZd07VuMPkQ","key=AIzaSyDqJd2-igPSOFN4hpP2j6x1pfaZPvRE81c","key=AIzaSyDM-wMlxXkfqywvDgEbM_SMfy_p1hyFqqk","key=AIzaSyDL9jh59ToSiqLUsK5yeCVPsKGkgDJ4Dj8"};
    public static String location="location=34.0522,-118.2437";
    public static String RADIUS="radius=";
    public static int VAL=0;
    public static final String symbol="&";
    public static StringBuffer response = new StringBuffer();
    public static String r;
    public static int i,j=0,k=0;
 public static void main(String args[]){
	 for(i=0;i<TYPE.length;i++)
	 {
		 System.out.println(TYPE[i]);
		 VAL=0;
		 j=i/9;
	 while(VAL <50000){

	 try {
		 VAL+=500;
		 String request=URL+urlGenerator(VAL,i,j);
		 //System.out.println(request);
		URL connection = new URL(request);
		HttpURLConnection con = (HttpURLConnection) connection.openConnection();
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		r= new String();
		String[] s;
		while ((inputLine = in.readLine()) != null) {
			
			r=r+(inputLine);
		}
		in.close();
		 //print result
		s=r.split("results");
		response.append(s[1]);
		//System.out.println(response.toString());
	} catch (IOException e) {
		e.printStackTrace();
	}
	 }
	 if(k%8==0)
	 {
	 try
	 {
		 String s=k+"data.txt";
	 File statText = new File(s);
     FileOutputStream is = new FileOutputStream(statText);
     OutputStreamWriter osw = new OutputStreamWriter(is);    
     Writer w = new BufferedWriter(osw);
     w.write(response.toString());
     w.close();
     response=new StringBuffer();
	 }
	 catch(Exception e)
	 {
	 System.out.println(e);
	 }
	 }
	 k++;
	 }
 }
 public static String urlGenerator(int value,int type,int key){
	 StringBuilder builder=new StringBuilder();
	 builder.append(location);
	 builder.append(symbol);
	 builder.append(RADIUS);
	 builder.append(value);
	 builder.append(symbol);
	 builder.append(TYPE[type]);
	 //System.out.println(TYPE[type]);
	 builder.append(symbol);
	 builder.append(KEY[key]);
	 return builder.toString();
 }
}
