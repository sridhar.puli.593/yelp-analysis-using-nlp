// Databricks notebook source exported at Tue, 31 May 2016 03:21:08 UTC

val data = sqlContext.sql("SELECT CAST(stars as DOUBLE) as label, text from CHECK")
    val splits = data.randomSplit(Array(0.80, 0.20), seed = 10)
///val test=splits(0).randomSplit(Array(0.8,0.20),seed=10)



// COMMAND ----------

//val res=data.map(x => (x(0).asInstanceOf[Double],x(1).toString.toLowerCase()))

// COMMAND ----------

 //val res=training.map(x => (x(0),processLine(x(1).toString,fullarray)))


//val splitnew=res.map(x => (x._1,x._2.split("\\n\\n")))

// COMMAND ----------

import org.apache.spark.ml.feature.RegexTokenizer
val tokenizer = new RegexTokenizer()
  .setPattern("\\p{L}+").setMinTokenLength(3)
.setGaps(false)
  .setInputCol("text")
  .setOutputCol("words")

// COMMAND ----------

val tokenized_df=tokenizer.transform(splits(0))

// COMMAND ----------

//tokenized_df.take(1)

// COMMAND ----------

// MAGIC  %sh wget http://ir.dcs.gla.ac.uk/resources/linguistic_utils/stop_words -O /tmp/stopwords

// COMMAND ----------

// MAGIC  %fs cp file:/tmp/stopwords dbfs:/tmp/stopwords

// COMMAND ----------

val stopwords = sc.textFile("/tmp/stopwords").collect()

// COMMAND ----------

import org.apache.spark.ml.feature.StopWordsRemover
// Set params for StopWordsRemover
val remover = new StopWordsRemover()
  .setStopWords(stopwords) // This parameter is optional
  .setInputCol("words")
  .setOutputCol("filtered")

// Create new DF with Stopwords removed
val filtered_df = remover.transform(tokenized_df)

// COMMAND ----------

filtered_df.take(2)

// COMMAND ----------

import org.apache.spark.ml.feature.Word2Vec
val word2Vec = new Word2Vec()
  .setInputCol("filtered")
  .setOutputCol("features")
  .setVectorSize(2)
  .setMinCount(0)

// COMMAND ----------

import org.apache.spark.ml.feature.CountVectorizer

// Set params for CountVectorizer
val vectorizer = new CountVectorizer()
  .setInputCol("filtered")
  .setOutputCol("features")
  .setVocabSize(1000)
  .setMinDF(2)
  .fit(filtered_df)

// COMMAND ----------

import org.apache.spark.ml.feature.{HashingTF, Tokenizer}

val hashingTF = new HashingTF()
  .setNumFeatures(1000)
  .setInputCol("filtered")
  .setOutputCol("features")
    



// COMMAND ----------

// val countVectors = vectorizer.transform(filtered_df).select("stars", "features")

// COMMAND ----------

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.PipelineStage
import org.apache.spark.ml.classification.NaiveBayes

val nb = new NaiveBayes()
nb.setModelType("multinomial") 
//val pipeline = new Pipeline().setStages(Array(tokenizer, remover, vectorizer,nb))

// COMMAND ----------

import org.apache.spark.mllib.classification.LogisticRegressionWithLBFGS

val model = new LogisticRegressionWithLBFGS()
  .setNumClasses(5) 

// COMMAND ----------

val pipeline = new Pipeline().setStages(Array(tokenizer,remover,vectorizer,nb))

// COMMAND ----------

val lrModel = pipeline.fit(splits(0))

// COMMAND ----------

val dd = lrModel.transform(splits(1))

// COMMAND ----------

val test = sqlContext.createDataFrame(Seq(
  (4, "roller coasters are fun"),
  (5, "This place is very bad and awful, I dont recommend this place"),
  (2, "I love this restaurant,like to visit again"),
  (3, "this place is kind of meh"),(2,"This place ambience is good,but service is very bad")
)).toDF("label", "text")

val results=lrModel.transform(test)
results.select("label","prediction").show()

// COMMAND ----------

//val dd=sqlContext.createDataFrame(Seq(
//  (4, "roller coasters are fun :-)"),
//  (5, "i burned very good :-("),
//  (6, "the place is very bad"),(5,"This place is awesome, I recommend this place"),(5,"I like this place but service is bad and awful and worst"),(5,"Very Good and awesome and excellent.Service is nice and friendly.Marvaleous,mind blowing")
//)).toDF("stars", "text")
//lrModel.transform(dd)
///val tokenized_df=tokenizer.transform(splits(1))
val tokenizer = new RegexTokenizer()
  .setPattern("\\p{L}+,\\W+").setMinTokenLength(3).setGaps(false)
  .setInputCol("text")
  .setOutputCol("filt")

val tokenized_df=tokenizer.transform(splits(1))
// Set params for StopWordsRemover
val remover = new StopWordsRemover()
  .setStopWords(stopwords) // This parameter is optional
  .setInputCol("filt")
  .setOutputCol("res")

// Create new DF with Stopwords removed
val filtered_df = remover.transform(tokenized_df)


val dd =lrModel.transform(filtered_df)
dd.collect()

// COMMAND ----------

val res=dd.select("label","prediction")

// COMMAND ----------

val doublrdd=res.map(x =>(x(0).asInstanceOf[Double],x(1).asInstanceOf[Double]))

// COMMAND ----------

1.0 *doublrdd.filter(x=>((x._1==x._2+1)||(x._1==x._2))).count()/splits(1).count()


// COMMAND ----------


